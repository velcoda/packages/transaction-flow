<?php

namespace Velcoda\TransactionFlow\UseCases;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Velcoda\Exceptions\Exceptions\HTTP_FORBIDDEN;
use Velcoda\Exceptions\Exceptions\NotImplementedException;

class Base
{
    private $request;
    private $data;
    protected $manualAuthorization = false;

    /**
     * @throws HTTP_FORBIDDEN
     */
    public function __construct(Request $request, $data = [])
    {
        $this->request = $request;
        $this->data = $data;
        if (!$this->manualAuthorization) {
            $this->authorize();
        }
    }

    protected function request()
    {
        return $this->request;
    }

    protected function parameter(string $name): string
    {
        return $this->request->route($name);
    }

    protected function query(string $name): array|string|null
    {
        return $this->request->query($name);
    }

    /**
     * @param string $name
     * @return UploadedFile|UploadedFile[]|array|null
     */
    protected function file(string $name): UploadedFile|array|null
    {
        return $this->request->file($name);
    }

    protected function token(): string|null
    {
        return $this->request->bearerToken();
    }

    protected function includes($name): bool
    {
        $includes = $this->query('includes');
        return in_array($name, $includes);
    }

    protected function apiKey(): string
    {
        return $this->request->header('x-api-key');
    }

    protected function usesApiKey(): string
    {
        return !!$this->request->header('x-api-key');
    }

    protected function has(string $name)
    {
        return key_exists($name, $this->data);
    }

    protected function hasFile(string $name)
    {
        return $this->request->hasFile($name);
    }

    protected function get(string $name)
    {
        return $this->data[$name]??null;
    }

    protected function authorize()
    {
        if ($this->usesApiKey()) {
            return;
        }
        if (!$this->authorizationRules()) {
            throw new HTTP_FORBIDDEN('Forbidden.');
        }
    }

    protected function authorizationRules(): bool
    {
        throw new NotImplementedException();
    }

    /**
     * @return mixed
     * @throws NotImplementedException
     */
    public function run()
    {
        throw new NotImplementedException();
    }
}
