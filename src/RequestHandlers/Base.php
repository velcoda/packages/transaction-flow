<?php

namespace Velcoda\TransactionFlow\RequestHandlers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Respect\Validation\Validator as v;
use Velcoda\Exceptions\Exceptions\HTTP_BAD_REQUEST;
use Velcoda\Exceptions\Exceptions\HTTP_INTERNAL_SERVER;

class Base
{
    protected Request $request;
    protected array $validated = [];

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->validateUrlParam();
        return $this->validateBody();
    }

    public function validated()
    {
        return $this->validated;
    }

    protected function parameter($key)
    {
        return $this->request->route($key);
    }

    protected function validateUrlParam()
    {
    }

    protected function validateBody(): array
    {
        return $this->validated;
    }

    protected function mandatory($key, $rule)
    {
        $this->check($key, $rule);
    }

    protected function optional($key, $rule)
    {
        if (!$this->request->has($key)) {
            return;
        }
        $this->check($key, $rule);
    }

    private function check($key, $rule)
    {
        if (!is_a($rule, Validator::class, true)) {
            throw new HTTP_INTERNAL_SERVER('Validator Invalid: ' . $key);
        }
        if (explode('.', $key) && !v::keyNested($key)->validate($this->request->all())) {
            throw new HTTP_BAD_REQUEST('Validation Error: ' . $key);
        }
        $value = $this->getValueByKey($key, $this->request->all());
        if (!$rule->validate($value)) {
            throw new HTTP_BAD_REQUEST('Validation Error: ' . $key);
        }
        $new_keys_arr = [];

        $keys = explode('.', $key);
        $reference = &$new_keys_arr;
        foreach ($keys as $key) {
            if (!array_key_exists($key, $reference)) {
                $reference[$key] = [];
            }
            $reference = &$reference[$key];
        }
        $reference = $value;
        unset($reference);

        $this->validated = array_merge_recursive($this->validated, $new_keys_arr);
    }

    private function getValueByKey($key, array $data)
    {
        // @assert $key is a non-empty string
        // @assert $data is a loopable array
        // @otherwise return $default value
        if (!is_string($key) || empty($key) || !count($data)) {
            return null;
        }

        // @assert $key contains a dot notated string
        if (strpos($key, '.') !== false) {
            $keys = explode('.', $key);

            foreach ($keys as $inner_key) {
                // @assert $data[$innerKey] is available to continue
                // @otherwise return $default value
                if (!is_array($data) || !array_key_exists($inner_key, $data)) {
                    return null;
                }
                $data = $data[$inner_key];
            }
            return $data;
        }

        // @fallback returning value of $key in $data or $default value
        return array_key_exists($key, $data) ? $data[$key] : null;
    }
}
