<?php

namespace Velcoda\TransactionFlow;

use Velcoda\Exceptions\Exceptions\HTTP_INTERNAL_SERVER;
use Velcoda\TransactionFlow\RequestHandlers\Base as RequestHandlerBase;
use Velcoda\TransactionFlow\UseCases\Base as UseCaseBase;

class TransactionFlow
{
    private ?string $requestHandler;
    private string $useCase;

    public static function new() {
        return new TransactionFlow();
    }

    public function request_handler(string $request_handler) {
        $this->requestHandler = $request_handler;
        return $this;
    }

    public function use_case(string $use_case) {
        $this->useCase = $use_case;
        return $this;
    }

    public function override_header(string $header, string $value) {
        $request = \request();
        $request->headers->set($header, $value);
        return $this;
    }

    public function run()
    {
        $request = \request();

        NewRelicReporting::set($request);

        if ($this->requestHandler) {
            if (!is_a($this->requestHandler, RequestHandlerBase::class, true)) {
                throw new HTTP_INTERNAL_SERVER('Not a valid request-handler!');
            }
        }

        if (!is_a($this->useCase, UseCaseBase::class, true)) {
            throw new HTTP_INTERNAL_SERVER('Not a valid use-case!');
        }
        if ($this->requestHandler) {
            $request_handler = new $this->requestHandler($request);
            return (new $this->useCase($request, $request_handler->validated()))->run();
        } else {
            return (new $this->useCase($request))->run();
        }
    }
}
