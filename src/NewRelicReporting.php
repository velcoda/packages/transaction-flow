<?php

namespace Velcoda\TransactionFlow;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Str;

class NewRelicReporting
{
    public static function set($request): void
    {
        if (extension_loaded('newrelic')) {
            newrelic_name_transaction(self::getTransactionName($request));
        }
    }

    /**
     * Example "GET|HEAD api/v1/user"
     *
     * @param Request $request
     * @return string
     */
    private static function getTransactionName(Request $request): string
    {
        $route = $request->route();
        $transactionName = '';

        if ($route instanceof Route) {
            $transactionName = Str::replaceFirst('App\\Http\\Controllers\\', '', $route->getActionName());
        }

        return $transactionName;
    }
}
